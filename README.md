# LibreCube / Platform / Communication / Wifi Module

With this module you can communicate with your remote segment over Wifi.

![](docs/image_01.jpg)

## How to Build

See [here](docs/assembly/README.md) for instructions.

## How to Modify

The `src` folder contains:

- the [FreeCAD](https://www.freecad.org/) source files for mechanical parts
- the firmware (MicroPython language) for the pyboard

## Contribute

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube Documentation](https://librecube.gitlab.io/).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The software is licensed under the MIT license.
The hardware is licensed under the CERN Open Hardware license.

See the [LICENSE](./LICENSE.txt) file for details.
