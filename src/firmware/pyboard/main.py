import time
import pyb

import spacecan
import spacecan.services

# from functions import perform_function


# hardware setup
led_heartbeat = pyb.LED(1)


# packet monitor
def packet_monitor(service, subtype, data, node_id):
    print(f"<-({service:02}, {subtype:02}) with [{data}] from {node_id}")


# create node and configure packet utilization protocol
responder = spacecan.Responder.from_file("config/spacecan.json")
pus = spacecan.services.PacketUtilizationServiceResponder(responder)

pus.packet_monitor = packet_monitor
# pus.parameter_management.add_parameters_from_file("config/parameters.json")
# pus.housekeeping.add_housekeeping_reports_from_file("config/housekeeping.json")


# pus.function_management.add_functions_from_file("config/functions.json")
# pus.function_management.perform_function = (
#     lambda function_id, arguments: perform_function(pus, function_id, arguments)
# )

responder.received_heartbeat = lambda: led_heartbeat.toggle()
responder.connect()
responder.start()


uart = pyb.UART(4, baudrate=115200, timeout=1000)
uart_A = pyb.UART(6, baudrate=115200, timeout=1000)
uart_B = pyb.UART(2, baudrate=115200, timeout=1000)


# main loop
try:
    print("Running...")
    while True:
        data = uart.readline()
        if data is None:
            continue
        print("Received:", data)
        uart_A.write(data)

except KeyboardInterrupt:
    pass

responder.stop()
responder.disconnect()
print("Stopped")
