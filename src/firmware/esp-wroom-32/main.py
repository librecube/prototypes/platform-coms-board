import network
import select
import socket
import asyncio
from machine import UART, Pin


uart = UART(2, baudrate=115200, timeout=1000)
led = Pin(2, Pin.OUT)


def setup_access_point(ssid, password):
    ap = network.WLAN(network.AP_IF)
    ap.active(True)
    ap.config(essid=ssid, password=password)
    while not ap.active():
        pass
    print("Network config:", ap.ifconfig())


def web_page():
    html = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ESP32 LED Control</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                text-align: center;
                background-color: #f0f0f0;
                margin: 0;
                padding: 20px;
            }
            h1 {
                color: #333;
            }
            .button-container {
                margin: 20px auto;
                display: flex;
                justify-content: center;
                flex-wrap: wrap;
            }
            .button {
                display: inline-block;
                padding: 15px 30px;
                margin: 10px;
                font-size: 18px;
                color: #fff;
                background-color: #007BFF;
                border: none;
                border-radius: 5px;
                cursor: pointer;
                transition: background-color 0.3s ease;
            }
            .button:hover {
                background-color: #0056b3;
            }
            .button.move {
                background-color: #28a745;
            }
            .button.stop {
                background-color: #dc3545;
            }
            footer {
                margin-top: 40px;
                font-size: 12px;
                color: #555;
            }
        </style>
    </head>
    <body>
        <h1>LibreCubeRover Control</h1>
        <div class="button-container">
            <button class="button stop" onclick="location.href='/stop'">Stop</button>
        </div>
        <div class="button-container">
            <button class="button move" onclick="location.href='/drive_forward'">Drive Forward</button>
        </div>
        <div class="button-container">
            <button class="button move" onclick="location.href='/drive_backward'">Drive Backward</button>
        </div>
        <div class="button-container">
            <button class="button move" onclick="location.href='/turn_left'">Turn Left</button>
            <button class="button move" onclick="location.href='/turn_right'">Turn Right</button>
        </div>
    </body>
    </html>
    """
    return html


async def handle_http_client(reader, writer):
    request = await reader.read(1024)
    led.value(not led.value())

    request_str = request.decode()
    print(f"HTTP Request: {request_str}")

    cmd = request_str.splitlines()[0]
    if "stop" in cmd:
        uart.write(b"80,8,1,1\n")
    elif "drive_forward" in cmd:
        uart.write(b"80,8,1,6\n")
        uart.write(b"80,8,1,2,100\n")
    elif "drive_backward" in cmd:
        uart.write(b"80,8,1,6\n")
        uart.write(b"80,8,1,3,100\n")
    elif "turn_left" in cmd:
        uart.write(b"80,8,1,4,100\n")
    elif "turn_right" in cmd:
        uart.write(b"80,8,1,5,100\n")

    response = web_page()
    writer.write("HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n")
    writer.write(response)
    await writer.drain()
    await writer.wait_closed()


async def start_http_server(host, port):
    try:
        server = await asyncio.start_server(handle_http_client, host, port)
        print(f"HTTP server running on {host}:{port}")
        async with server:
            await server.wait_closed()
    except Exception as e:
        print(f"Error in HTTP server: {e}")


async def start_udp_server(host, port):
    TIMEOUT = 1
    MAX_PACKET = 1024
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setblocking(False)
        sock.bind((host, port))
        print(f"UDP server running on {host}:{port}")

        polling = select.poll()
        polling.register(sock, select.POLLIN)

        def callback(msg, addr):
            led.value(not led.value())
            print(msg)
            uart.write(msg)

        while True:
            if polling.poll(TIMEOUT):
                msg, addr = sock.recvfrom(MAX_PACKET)
                callback(msg, addr)
                await asyncio.sleep(0)
            await asyncio.sleep(0)

    except Exception as e:
        print(f"Error in UDP server: {e}")


async def main():
    setup_access_point("LibreCubeRover", "123456789")
    task1 = asyncio.create_task(start_udp_server("0.0.0.0", 5555))
    task2 = asyncio.create_task(start_http_server("0.0.0.0", 8080))
    await asyncio.gather(task1, task2)


try:
    asyncio.run(main())
except Exception as e:
    print(f"Error in main loop: {e}")
